package com.gitlab.ermax81.tutorial;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;

public class LocalDate2Date {

    //Source: https://www.javacodegeek.com/2020/07/localdate-to-date-in-java.html

    @DisplayName("LocalDate to Date")
    @Test
    void localdate_2_date() {
        //Creating LocalDate instance
        LocalDate now = LocalDate.now();
        //Creating ZonedDateTime instance
        ZonedDateTime zdt = now.atStartOfDay(ZoneId.systemDefault());
        //Creating Instant instance
        Instant instant = zdt.toInstant();
        //Creating Date instance using instant instance.
        Date date = Date.from(instant);
        System.out.println("LocalDate To (java.util.)Date :  " + date);
    }
}
