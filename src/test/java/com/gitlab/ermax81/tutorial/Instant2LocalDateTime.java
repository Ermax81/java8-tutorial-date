package com.gitlab.ermax81.tutorial;

import org.junit.jupiter.api.*;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;

public class Instant2LocalDateTime {

    //Source: https://www.javacodegeek.com/2020/07/localdatetime-to-instant.html

    @BeforeAll
    static void setup(){
        System.out.println("@BeforeAll executed");
    }

    @BeforeEach
    void setupThis(){
        System.out.println("@BeforeEach executed");
    }

    @AfterEach
    void tearThis(){
        System.out.println("@AfterEach executed");
    }

    @AfterAll
    static void tear(){
        System.out.println("@AfterAll executed");
    }

    @DisplayName("Instant to LocalDateTime")
    @Test
    void instant_2_localdatetime() {
        Instant instant = Instant.now();
        System.out.println("Instant: "+instant);

        // Using LocalDateTime.ofInstant() method
        LocalDateTime time =
                LocalDateTime.ofInstant(instant, ZoneId.systemDefault()); //Take zone from system
        System.out.println("LocalDateTime: "+time);

        // Using Timestamp.from(Instant).toLocalDateTime()
        LocalDateTime time1 =
                Timestamp.from(instant).toLocalDateTime();
        System.out.println("Timestamp.from(Instant).toLocalDateTime: "+time1);
    }

    @DisplayName("LocalDateTime to Instant")
    @Test
    void localdatetime_2_instant() {
        LocalDateTime localdatetime = LocalDateTime.now();
        System.out.println("LocalDateTime now: "+localdatetime);

        // Using LocalDateTime.toInstant method
        Instant instant =
                localdatetime.toInstant(ZoneOffset.UTC);
        System.out.println("LocalDateTime.toInstant(ZoneOffset.UTC): "+instant);

        // Using LocalDateTime.atZone().toInstant()
        Instant instant1 = localdatetime
                .atZone(ZoneId.systemDefault()).toInstant();
        System.out.println("Instant (LocalDateTime.atZone(ZoneId).toInstant): "+instant1);

        // Using Instant.ofEpochSecond
        long timeInSeconds =
                localdatetime.toEpochSecond(ZoneOffset.UTC);
        Instant instant2 =
                Instant.ofEpochSecond(timeInSeconds);
        System.out.println("timeInSeconds"+ instant2);
    }

}
