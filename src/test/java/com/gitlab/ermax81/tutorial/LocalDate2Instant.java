package com.gitlab.ermax81.tutorial;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.*;

public class LocalDate2Instant {

    //Source: https://www.javacodegeek.com/2020/07/localdate-to-instant.html

    @DisplayName("LocalDate to Instant")
    @Test
    void localdate_2_instant() {

        //Using LocalDate.atStartOfDay().atZone()
        LocalDate localDate = LocalDate.now();
        Instant instant1 = localDate.atStartOfDay()
                .atZone(ZoneId.of("Europe/Paris")).toInstant();
        System.out.println("Using LocalDate.atStartOfDay().atZone() : "+instant1);

        //Using LocalDate.atStartOfDay(ZoneId.systemDefault())
        LocalDate localDate1 = LocalDate.now();
        Instant instant2 = localDate1.atStartOfDay(ZoneId.systemDefault())
                .toInstant();
        System.out.println("Using LocalDate.atStartOfDay(ZoneId.systemDefault()) : "+ instant2);

        //Using LocalDate.atTime()
        LocalDate localDate2 = LocalDate.now();
        Instant instant3 = localDate2.atTime(LocalTime.now())
                .toInstant(ZoneOffset.UTC);
        System.out.println("Using LocalDate.atTime() : "+ instant3);

        //Using LocalDate.atTime(LocalTime.now()).atZone()
        LocalDate localDate3 = LocalDate.now();
        Instant instant4 = localDate3.atTime(LocalTime.now())
                .atZone(ZoneId.systemDefault()).toInstant();
        System.out.println("Using LocalDate.atTime(LocalTime.now()).atZone() : "+ instant4);

    }
}
