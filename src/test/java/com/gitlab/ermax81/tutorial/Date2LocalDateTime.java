package com.gitlab.ermax81.tutorial;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.*;
import java.util.Date;

public class Date2LocalDateTime {

    //Source: https://www.javacodegeek.com/2020/06/java-date-to-localdatetime.html

    @DisplayName("Date to LocalDateTime (meth1)")
    @Test
    void date_2_localdatetime_meth1() {
        // Creating Date Instance
        Date today = new Date();

        // Creating Instant instance
        Instant instant = today.toInstant();

        // Getting ZoneId instance
        ZoneId systemDefaultZone = ZoneId.systemDefault();

        // Creating LocalDateTime instance
        LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, systemDefaultZone);
        System.out.println("Date to LocalDateTime : " + localDateTime);
        // Prints : Date to LocalDateTime : 2020-06-27T21:18:53.831
    }

    @DisplayName("Date to LocalDateTime (meth2)")
    @Test
    void date_2_localdatetime_meth2() {
        // Creating Date Instance
        Date today = new Date();

        // Getting default timezone instance
        ZoneId systemDefaultZone = ZoneId.systemDefault();

        // Creating LocalDateTime instance
        LocalDateTime localDateTime = Instant.ofEpochMilli(today.getTime())
                .atZone(systemDefaultZone).toLocalDateTime();
        System.out.println("Date to LocalDateTime : " + localDateTime);
        // Prints : Date to LocalDateTime : 2020-06-27T21:18:53.831
    }
}
