package com.gitlab.ermax81.tutorial;

import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

public class DurationBetween {

    @Test
    void duration_between_2_instant() {
        Instant now = Instant.now();
        Instant tomorrow = now.plus( 1, ChronoUnit.DAYS );
        Duration delay = Duration.between( now, tomorrow );
        System.out.println("Duration: "+delay);
        System.out.println("Duration (days): (long) "+delay.toDays());

        delay = Duration.between( now, now.plus(12, ChronoUnit.HOURS));
        System.out.println("Duration (days): (long) "+delay.toDays());

    }
}
