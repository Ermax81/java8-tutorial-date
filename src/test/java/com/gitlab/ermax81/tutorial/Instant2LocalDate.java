package com.gitlab.ermax81.tutorial;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.*;

public class Instant2LocalDate {

    //Source: https://www.javacodegeek.com/2020/07/instant-to-localdate.html

    @DisplayName("Instant to LocalDate")
    @Test
    void instant_2_localdate() {
        //Using Instant.atZone()
        Instant instant1 = Instant.now();
        LocalDate localDate1 = instant1.atZone(ZoneId.systemDefault())
                .toLocalDate();
        System.out.println("Using Instant.atZone() : "+ localDate1);//2020-07-07

        //Using Instant.atOffset()
        Instant instant2 = Instant.now();
        LocalDate localDate2 = instant2.atOffset(ZoneOffset.UTC)
                .toLocalDate();
        System.out.println("Using Instant.atOffset() : "+localDate2);//2020-07-07

        //Using LocalDateTime.ofInstant()
        Instant instant3 = Instant.now();
        LocalDate localDate3= LocalDateTime.ofInstant(instant3, ZoneOffset.UTC)
                .toLocalDate();
        System.out.println("Using LocalDateTime.ofInstant() : "+localDate3);//2020-07-07
    }
}
