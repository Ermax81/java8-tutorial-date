package com.gitlab.ermax81.tutorial;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class LocalDate2LocalDateTime {

    //Source: https://www.javacodegeek.com/2020/06/localdate-to-localdatetime.html

    @DisplayName("LocalDate to LocalDateTime (meth1)")
    @Test
    void localdate_2_localdatetime_meth1() {
        // Gives LocalDate instance
        LocalDate todaysDate = LocalDate.now();

        // atStartOfDay will give date with timestamp 00:00 in LocalDateTimeFormat
        LocalDateTime atStartOfDay = todaysDate.atStartOfDay();
        System.out.println("LocalDateTime atStartOfDay : " + atStartOfDay);
        // Prints LocalDateTime atStartOfDay : 2020-06-30T00:00
    }

    @DisplayName("LocalDate to LocalDateTime (meth2)")
    @Test
    void localdate_2_localdatetime_meth2() {
        // Gives LocalDate instance
        LocalDate todaysDate = LocalDate.now();

        // Gives LocalDateTime using LocalTime.
        LocalDateTime atLocalTime = todaysDate.atTime(LocalTime.now());
        System.out.println("LocalDateTime using LocalTime : " + atLocalTime);
        // Prints LocalDateTime using LocalTime : 2020-06-30T14:08:18.484
    }

    @DisplayName("LocalDate to LocalDateTime (meth3)")
    @Test
    void localdate_2_localdatetime_meth3() {
        // Gives LocalDate instance
        LocalDate todaysDate = LocalDate.now();

        // Gives LocalDateTime at specific hour and minute
        LocalDateTime atTime = todaysDate.atTime(23, 00);
        System.out.println("LocalDateTime using specific hour and minute : " + atTime);
        // Prints LocalDateTime using specific hour and minute : 2020-06-30T23:00

    }

    @DisplayName("LocalDate to LocalDateTime (meth4)")
    @Test
    void localdate_2_localdatetime_meth4() {
        //Gives LocalDate instance
        LocalDate todaysDate = LocalDate.now();

        //Gives LocalDateTime at specific hour,minute and seconds
        LocalDateTime atTimeWithSeconds = todaysDate.atTime(23, 00, 15);
        System.out.println("LocalDateTime using specific hour, minute, seconds : " + atTimeWithSeconds);
        // Prints LocalDateTime using specific hour, minute, seconds :
        // 2020-06-30T23:00:15
    }

    @DisplayName("LocalDate to LocalDateTime (meth5)")
    @Test
    void localdate_2_localdatetime_meth5() {
        //Gives LocalDate instance
        LocalDate todaysDate = LocalDate.now();

        //Gives LocalDateTime at specific hour,minute,second,nanoseconds
        LocalDateTime atTimeWithNanoSeconds = todaysDate.atTime(23, 00, 15, 2345);
        System.out.println("LocalDateTime using specific hour, minute, seconds, nanoseconds : " + atTimeWithNanoSeconds);
        // Prints LocalDateTime using specific hour, minute, seconds, nanoseconds :
        // 2020-06-30T23:00:15.000002345
    }
}
