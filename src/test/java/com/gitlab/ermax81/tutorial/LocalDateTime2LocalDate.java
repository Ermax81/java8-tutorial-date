package com.gitlab.ermax81.tutorial;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.*;
import java.util.Date;

public class LocalDateTime2LocalDate {

    //Source: https://www.javacodegeek.com/2020/06/localdatetime-to-localdate.html

    @DisplayName("LocalDateTime to LocalDate")
    @Test
    void localdatetime_2_localdate() {
        // Creating LocalDateTime instance
        LocalDateTime currentDateTime = LocalDateTime.now();
        System.out.println("CurrentDateTime : " + currentDateTime);

        // Creating LocalDate instance
        LocalDate currentDate = currentDateTime.toLocalDate();

        // Prints LocalDateTime to LocalDate value
        System.out.println("LocalDateTime to LocalDate : " + currentDate);
    }
}
