package com.gitlab.ermax81.tutorial;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.sql.Timestamp;
import java.time.*;
import java.util.Date;

public class LocalDateTime2Date {

    //Source: https://www.javacodegeek.com/2020/07/localdatetime-to-date.html

    @DisplayName("LocalDateTime to Date (meth1)")
    @Test
    void localDateTime_2_date_meth1() {

        // Creating LocalDateTime instance
        LocalDateTime time = LocalDateTime.now();
        System.out.println("LocalDateTime value : " + time);

        // Creating Instant instance from localDateTime time
        Instant instant = time.atZone(ZoneId.systemDefault()).toInstant();

        // Creating date from instant instance.
        Date date = Date.from(instant);

        // Printing date
        System.out.println("Date value " + date); // Wed Jul 01 19:34:42 IST 2020

    }

    @DisplayName("LocalDateTime to Date (meth2)")
    @Test
    void localdatetime_2_date_meth2() {
        //Source: https://mkyong.com/java8/java-convert-instant-to-localdatetime/

        // Hard code a date time
        LocalDateTime dateTime = LocalDateTime.of(2021, Month.MARCH, 17, 6, 17, 10);

        System.out.println("LocalDateTime : " + dateTime);

        // Convert LocalDateTime to Instant, UTC+0
        Instant instant = dateTime.toInstant(ZoneOffset.UTC);

        System.out.println("Instant : " + instant);
    }

    @DisplayName("LocalDateTime to Date (meth3)")
    @Test
    void localdatetime_2_date_meth3() {
        // Parse a ISO 8601 Date directly
        //Instant instant = Instant.parse("2016-08-18T06:17:10.225Z");

        Instant instant = Instant.now();

        System.out.println("Instant : " + instant);

        //Convert instant to LocalDateTime, no timezone, add a zero offset / UTC+0
        LocalDateTime ldt = LocalDateTime.ofInstant(instant, ZoneOffset.UTC);

        System.out.println("LocalDateTime : " + ldt);
    }
}
