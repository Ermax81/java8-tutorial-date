package com.gitlab.ermax81.tutorial;

import org.junit.jupiter.api.Test;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;

public class JavaInstantNow {

    //Source: https://www.javacodegeek.com/2020/07/java-instant-now.html

    @Test
    void javaInstantNow() {
        // java instant now example
        Instant instant = Instant.now();
        System.out.println("Instant now() : " + instant);
    }

    @Test
    void javaInstantNowClock() {
        // java instant now using clock
        Instant instant =
                Instant.now(Clock.systemDefaultZone());
        System.out.println("Java Instant now Clock : "
                + instant);
    }

    @Test
    void javaInstantNowUTC() {
        // Java Instant Now UTC
        Instant instant = Instant.now(Clock.systemUTC());
        System.out.println("Java Instant now UTC : " + instant);
    }

    @Test
    void javaInstantNowTimezone() {
        Instant instant = Instant.now();
        System.out.println("Instant : " + instant);

        //Java Instant now with custome Timezone
        ZonedDateTime time =
                instant.atZone(ZoneId.of("Europe/Paris"));
        System.out.println("Java Instant now Timezone : "
                + time);
    }

    @Test
    void javaInstantNowFormat() {
        Instant instant = Instant.now();

        // Java Instant now with format example
        DateTimeFormatter formatter =
                DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")
                        .withZone(ZoneId.systemDefault());

        System.out.println("Java Instant Now Format : "
                + formatter.format(instant));
    }

    @Test
    void javaInstantNowMinusOneDay() {
        Instant instant = Instant.now();
        System.out.println("Instant : " + instant);

        //Java Instant now Minus One Day
        Instant instantMinusOneDay =
                instant.minus(1, ChronoUnit.DAYS);

        System.out.println("Java Instant Now Minus One Day : "
                + instantMinusOneDay);
    }

    @Test
    void javaInstantNowNanoSeconds() {
        Instant instant = Instant.now();
        System.out.println("Instant : " + instant);

        //Java Instant now Nanoseconds
        int nanoseconds = instant.getNano();

        System.out.println("Java Instant now to Nanoseconds : "
                + nanoseconds);
    }

    @Test
    void javaInstantNowToDate() {
        Instant instant = Instant.now();
        System.out.println("Instant : " + instant);

        // Java Instant now to Date
        Date date = Date.from(instant);
        System.out.println("Java Instant now to Date : " + date);
    }

}
