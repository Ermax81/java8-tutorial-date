package com.gitlab.ermax81.tutorial;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;

public class Date2LocalDate {

    //Source: https://www.javacodegeek.com/2020/07/date-to-localdate-java.html

    @DisplayName("Date to LocalDate (meth1)")
    @Test
    void date_2_localdate_meth1() {
        // Creating Date instance
        Date date = new Date();
        // Creating Instant instance
        Instant instant = date.toInstant();
        // Creating ZonedDateTime instance
        ZonedDateTime zdt = instant.atZone(ZoneId.systemDefault());
        // Creating LocalDate instance
        LocalDate localDate = zdt.toLocalDate();
        System.out.println("Date to LocalDate : " + localDate);
    }

    @DisplayName("Date to LocalDate (meth2)")
    @Test
    void date_2_localdate_meth2() {
        // Creating Date instance
        Date date = new Date();
        // Creating Instant instance using ofEpcohMilli method
        Instant instant = Instant.ofEpochMilli(date.getTime());
        // Creating ZonedDateTime instance
        ZonedDateTime zdt = instant.atZone(ZoneId.systemDefault());
        // Creating localdate instance
        LocalDate localdate = zdt.toLocalDate();
        System.out.println("Date to LocalDate : " + localdate);
    }

    @DisplayName("Date to LocalDate (meth3)")
    @Test
    void date_2_localdate_meth3() {
        // Creating Date instance
        Date date = new Date();
        //Creating LocalDate instance using sql Date class toLocalDate method.
        LocalDate localDate = new java.sql.Date(date.getTime()).toLocalDate();
        System.out.println("Date to LocalDate : " + localDate);
    }
}
